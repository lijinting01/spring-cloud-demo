package cc.picc;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 
 * @author lijinting01
 *
 */
@SpringBootApplication
@EnableEurekaServer
public class DiscoveryDemoApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(DiscoveryDemoApplication.class).web(true).run(args);
	}
}
