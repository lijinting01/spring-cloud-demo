package com.example;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.entity.User;
import com.example.repository.UserMapper;

/**
 * 
 * @author lijinting01
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

	@Autowired
	private UserMapper userMapper;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	@Rollback
	public void test() {
		userMapper.insert(15L, "AAA", new Date());
		User u = userMapper.findByName("AAA");
		Assert.assertEquals("AAA", u.getName());
	}

}
