package com.example.repository;

import java.util.Date;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.entity.User;

/**
 * 
 * @author lijinting01
 *
 */
@Mapper
public interface UserMapper {

	/**
	 * 
	 * @param name
	 * @return
	 */
	@Select("SELECT * FROM t_USER WHERE NAME = #{name}")
	User findByName(@Param("name") String name);

	/**
	 * 
	 * @param id
	 * @param name
	 * @param birthDate
	 * @return
	 */
	@Insert("INSERT INTO t_USER(ID, NAME, BIRTHDATE) VALUES (#{id}, #{name}, #{birthDate})")
	int insert(@Param("id") long id, @Param("name") String name, @Param("birthDate") Date birthDate);
}
