package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * 
 * @author lijinting01
 *
 */
@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class ConsumeDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumeDemoApplication.class, args);
	}
}
