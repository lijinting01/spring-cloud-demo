package com.example.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author lijinting01
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Campaign {

	private Long id;

	private String campaignName;

	private Date deadline;

	private Integer goal;

	@Override
	public String toString() {
		return String.format("Campaign [id=%s, campaignName=%s, deadline=%s, goal=%s]", id, campaignName, deadline,
				goal);
	}

	public Long getId() {
		return id;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public Date getDeadline() {
		return deadline;
	}

	public Integer getGoal() {
		return goal;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public void setGoal(Integer goal) {
		this.goal = goal;
	}

}
