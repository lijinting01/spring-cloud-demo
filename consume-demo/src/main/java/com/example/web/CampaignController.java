package com.example.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.Campaign;
import com.example.service.CampaignService;

/**
 * 
 * @author lijinting01
 *
 */
@RestController
@RequestMapping(value = "/campaign")
public class CampaignController {

	/**
	 * 
	 */
	private CampaignService campaignService;

	/**
	 * 
	 * @param campaignService
	 */
	@Autowired
	public CampaignController(CampaignService campaignService) {
		this.campaignService = campaignService;
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/query", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
	public List<Campaign> query() {
		return campaignService.queryAll();
	}
}
