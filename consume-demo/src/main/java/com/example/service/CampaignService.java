package com.example.service;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.domain.Campaign;

/**
 * 
 * @author lijinting01
 *
 */
@FeignClient("proc-demo")
@FunctionalInterface
public interface CampaignService {
	/**
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/api/campaign/queryAll")
	List<Campaign> queryAll();

}
