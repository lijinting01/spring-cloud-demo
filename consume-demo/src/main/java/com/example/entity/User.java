package com.example.entity;

import java.util.Date;

/**
 * 
 * @author lijinting01
 *
 */
public class User {

	private Long id;

	private String name;

	private Date birthDate;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

}
