package cc.picc.web;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cc.picc.commons.InconsistentInformationException;
import cc.picc.mkt.entity.VehicleRenewCampaignObjectEntity;
import cc.picc.mkt.service.VehicleRenewCampaignService;

/**
 * 
 * @author Justin
 *
 */
@RestController
@RequestMapping(value = "/api/campaign/vehicle/renew")
public class VehicleRenewCampaignController {

	private VehicleRenewCampaignService vehicleRenewCampaignService;

	/**
	 * 
	 * @param vehicleRenewCampaignService
	 */
	@Autowired
	public VehicleRenewCampaignController(VehicleRenewCampaignService vehicleRenewCampaignService) {
		this.vehicleRenewCampaignService = vehicleRenewCampaignService;
	}

	/**
	 * 加入活动
	 * 
	 * @param campaignId
	 * @param customerId
	 *            客户id
	 * @param policyNo
	 *            保单号
	 * @return
	 */
	@RequestMapping(value = { "/join" }, method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	@ResponseStatus(value = HttpStatus.OK)
	public Map<String, Object> joinCampaign(@RequestParam(name = "campaignId") Long campaignId,
			@RequestParam(name = "customerId") Long customerId, @RequestParam(name = "policyNo") String policyNo) {
		String procIns = vehicleRenewCampaignService.joinCampaign(campaignId, customerId, policyNo);
		return Collections.singletonMap("processInstanceId", procIns);
	}

	/**
	 * 查询活动对象
	 * 
	 * @param query
	 * @return
	 */
	@RequestMapping(value = {
			"/query" }, method = RequestMethod.POST, produces = "application/json;charset=UTF-8", consumes = "application/json;charset=UTF-8")
	@ResponseStatus(value = HttpStatus.OK)
	public List<VehicleRenewCampaignObjectEntity> queryCampaign(@RequestBody CampaignObjectQuery query) {
		Page<VehicleRenewCampaignObjectEntity> result = vehicleRenewCampaignService
				.findVehicleRenewCampaignObjectsByCampaign(query.getCampaignName(), query.getPage(), query.getSize());
		return result.getContent();
	}

	/**
	 * 缺少数据
	 * 
	 * @param e
	 * @return
	 */
	@ExceptionHandler(EmptyResultDataAccessException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Problem handleEmptyResultDataAccessException(EmptyResultDataAccessException e) {
		return new Problem(-5001, String.format("class:%s, message:%s, stacktrace:%s", e.getClass(), e.getMessage(),
				Arrays.toString(e.getStackTrace())));
	}

	/**
	 * 信息不一致——先决条件不匹配，不符合业务约束。
	 * 
	 * @param e
	 * @return
	 */
	@ExceptionHandler(InconsistentInformationException.class)
	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	public Problem handleInconsistentInformationException(InconsistentInformationException e) {
		return new Problem(-5002, String.format("class:%s, message:%s, stacktrace:%s", e.getClass(), e.getMessage(),
				Arrays.toString(e.getStackTrace())));
	}

	static class CampaignObjectQuery {
		private String campaignName;
		private int page;
		private int size;

		public String getCampaignName() {
			return campaignName;
		}

		public int getPage() {
			return page;
		}

		public int getSize() {
			return size;
		}

		public void setCampaignName(String campaignName) {
			this.campaignName = campaignName;
		}

		public void setPage(int page) {
			this.page = page;
		}

		public void setSize(int size) {
			this.size = size;
		}

	}

}
