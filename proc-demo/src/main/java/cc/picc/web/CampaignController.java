package cc.picc.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cc.picc.mkt.entity.BranchCampaignEntity;
import cc.picc.mkt.repository.BranchCampaignRepository;

/**
 * 
 * @author Justin
 *
 */
@RestController
@RequestMapping(value = "/api/campaign")
public class CampaignController {

	private BranchCampaignRepository branchCampaignRepo;

	/**
	 * 
	 * @param branchCampaignRepo
	 */
	@Autowired
	public CampaignController(BranchCampaignRepository branchCampaignRepo) {
		this.branchCampaignRepo = branchCampaignRepo;
	}

	/**
	 * 
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value = "/queryAll", produces="application/json;charset=UTF-8")
	public List<BranchCampaignEntity> queryAllBranchCampaign() {
		Page<BranchCampaignEntity> p = branchCampaignRepo.findAll(new PageRequest(0, 1000));
		return p.getContent();
	}
}
