package cc.picc.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 
 * @author lijinting01
 *
 */
@ControllerAdvice
public class ProblemHandler {

	/**
	 * 
	 */
	private static final String ERROR_VIEW = "/error";

	/**
	 * 
	 * @param req
	 * @param ex
	 * @param model
	 * @return
	 */
	@ExceptionHandler(value = Exception.class)
	public String handleException(HttpServletRequest req, Throwable ex, Model model) {
		model.addAttribute("prev", req.getHeader("Referer"));
		model.addAttribute("summary", "错误");
		model.addAttribute("message", ex.getMessage());
		return ERROR_VIEW;
	}

}
