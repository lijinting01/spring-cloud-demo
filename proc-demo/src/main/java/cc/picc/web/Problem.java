package cc.picc.web;

/**
 * 
 * @author Justin
 *
 */
public class Problem {
	private int code;

	private String message;

	/**
	 * 
	 * @param code
	 * @param message
	 */
	public Problem(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
