package cc.picc.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author Justin
 *
 */
@Controller
@RequestMapping(value = "/")
public class HomeController {

	private static final String VIEW_HOMEPAGE = "home";

	/**
	 * 
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String home() {
		return VIEW_HOMEPAGE;
	}
}
