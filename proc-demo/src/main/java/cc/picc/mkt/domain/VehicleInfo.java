package cc.picc.mkt.domain;

import java.util.Date;

/**
 * 车辆信息
 * 
 * @author lijinting01
 *
 */
public interface VehicleInfo {

	/**
	 * 
	 * @return
	 */
	Long getId();

	/**
	 * 车牌号
	 * 
	 * @return
	 */
	String getLicenseNumber();

	/**
	 * 车架号
	 * 
	 * @return
	 */
	String getFrameNumber();

	/**
	 * 
	 * @return
	 */
	String getEngineNumber();

	/**
	 * 初登日期
	 * 
	 * @return
	 */
	Date getRegisteredDate();

}
