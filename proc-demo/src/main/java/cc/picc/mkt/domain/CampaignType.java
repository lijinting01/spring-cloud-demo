package cc.picc.mkt.domain;

/**
 * 营销活动类型
 * 
 * @author Justin
 *
 */
public enum CampaignType {
	/**
	 * 车险续保
	 */
	VEHICLE_RENEW,

	/**
	 * 车险新保
	 */
	VEHICLE_ACQUISITION,

	/**
	 * 个人客户关怀
	 */
	INDIVIDUAL_GREETING;
}