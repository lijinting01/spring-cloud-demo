package cc.picc.mkt.domain;

/**
 * 分公司层级信息
 * 
 * @author Justin
 *
 */
@FunctionalInterface
public interface BranchLevelInfo {
	/**
	 * 
	 * @return
	 */
	BranchLevel getBranchLevel();
}
