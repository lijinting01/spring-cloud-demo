package cc.picc.mkt.domain;

import java.util.Date;

/**
 * 续保活动对象
 * 
 * @param <E>
 * @author lijinting01
 *
 */
public interface RenewCampaignObjectInfo<E extends OrganizationInfo> {

	/**
	 * 活动对象的标识
	 * 
	 * @return
	 */
	Long getId();

	/**
	 * 活动信息
	 * 
	 * @return
	 */
	CampaignInfo<E> getCampaign();

	/**
	 * 客户信息
	 * 
	 * @return
	 */
	CustomerInfo getCustomer();

	/**
	 * 保单信息
	 * 
	 * @return
	 */
	PolicyInfo getPolicy();

	/**
	 * 添加时间
	 * 
	 * @return
	 */
	Date getInsertTime();
}
