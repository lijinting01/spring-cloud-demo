package cc.picc.mkt.domain;

/**
 * 
 * @author Justin
 *
 */
public enum BranchLevel {
	/**
	 * 总公司
	 */
	NATIONAL,

	/**
	 * 省级公司
	 */
	PROVINCIAL,

	/**
	 * 地市级
	 */

	MUNICIPAL,

	/**
	 * 县支级
	 */
	REGIONAL,

	/**
	 * 营业部
	 */
	RETAILER;
}
