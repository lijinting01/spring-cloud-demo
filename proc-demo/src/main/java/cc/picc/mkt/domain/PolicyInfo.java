package cc.picc.mkt.domain;

import java.util.Date;

/**
 * 
 * @author Justin
 *
 */
public interface PolicyInfo {

	/**
	 * 终保日期
	 * 
	 * @return
	 */
	Date getEndDate();

	/**
	 * 起保日期
	 * 
	 * @return
	 */
	Date getStartDate();

	/**
	 * 险种代码
	 * 
	 * @return
	 */
	String getRiskCode();

	/**
	 * 产品线代码
	 * 
	 * @return
	 */
	String getClassCode();

	/**
	 * 投保单号
	 * 
	 * @return
	 */
	String getProposalNo();

	/**
	 * 保单号
	 * 
	 * @return
	 */
	String getPolicyNo();
}
