package cc.picc.mkt.domain;

/**
 * 机构信息
 * 
 * @author Justin
 *
 */
public interface OrganizationInfo {
	/**
	 * 
	 * @return
	 */
	Long getId();

	/**
	 * 机构名称
	 * 
	 * @return
	 */
	String getOrganizationName();

	/**
	 * 机构代码
	 * 
	 * @return
	 */
	String getOrganizationCode();

	/**
	 * 
	 * @return
	 */
	OrganizationType getOrganizationType();
}
