package cc.picc.mkt.domain;

/**
 * 客户类型
 * 
 * @author lijinting01
 *
 */
public enum CustomerType {
	/**
	 * 个人客户
	 */
	INDIVIDUAL,

	/**
	 * 团体客户
	 */
	GROUP;

}
