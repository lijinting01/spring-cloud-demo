package cc.picc.mkt.domain;

import java.util.Date;
import java.util.List;

/**
 * 营销活动
 * 
 * @param <E>实际类型
 * @author Justin
 *
 */
public interface CampaignInfo<E extends OrganizationInfo> {
	/**
	 * 
	 * @return
	 */
	Long getId();

	/**
	 * 活动机构。
	 * 
	 * @return
	 */
	List<E> getOrganizations();

	/**
	 * 活动名称
	 * 
	 * @return
	 */
	String getCampaignName();

	/**
	 * 活动类型
	 * 
	 * @return
	 */
	CampaignType getCampaignType();

	/**
	 * 活动截止时间。必须晚于当前时间。
	 * 
	 * @return
	 */
	Date getDeadline();

	/**
	 * 销售目标，不允许为负值，单位为“万”元。
	 * 
	 * @return
	 */
	Integer getGoal();
}
