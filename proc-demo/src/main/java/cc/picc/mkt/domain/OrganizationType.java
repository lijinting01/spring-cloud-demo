package cc.picc.mkt.domain;

/**
 * 
 * @author Justin
 *
 */
public enum OrganizationType {
	/**
	 * 子公司
	 */
	SUBSIDIARY,

	/**
	 * 分公司
	 */
	BRANCH,

	/**
	 * 部门
	 */
	DEPARTMENT,

	/**
	 * 外部机构
	 */
	AGENT;
}
