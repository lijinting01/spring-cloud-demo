package cc.picc.mkt.domain;

/**
 * 
 * @author Justin
 *
 */
@FunctionalInterface
public interface VehicleRenewCampaignObjectInfo {
	/**
	 * 车辆信息
	 * 
	 * @return
	 */
	VehicleInfo getVehicle();
}
