package cc.picc.mkt.domain;

/**
 * 
 * @author lijinting01
 *
 */
public interface CustomerInfo {
	/**
	 * 
	 * @return
	 */
	Long getId();

	/**
	 * 
	 * @return
	 */
	CustomerType getCustomerType();

	/**
	 * 
	 * @return
	 */
	String getCustomerName();
}
