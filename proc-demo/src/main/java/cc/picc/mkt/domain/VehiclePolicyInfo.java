package cc.picc.mkt.domain;

/**
 * 车险保单信息
 * 
 * @author lijinting01
 *
 */
public interface VehiclePolicyInfo extends PolicyInfo {

	/**
	 * 
	 * @return
	 */
	Long getId();

	/**
	 * 
	 * @return
	 */
	VehicleInfo getVehicle();

	/**
	 * 
	 * @return
	 */
	CustomerInfo getCustomer();

}
