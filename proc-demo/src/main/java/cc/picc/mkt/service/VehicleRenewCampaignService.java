package cc.picc.mkt.service;

import org.springframework.data.domain.Page;

import cc.picc.mkt.entity.VehicleRenewCampaignObjectEntity;

/**
 * 车险续保活动服务
 * 
 * @author lijinting01
 *
 */
public interface VehicleRenewCampaignService {

	/**
	 * @param campaignId
	 * @param customerId
	 * @param policyId
	 * @param cb
	 * @return processInstance的id
	 */
	String joinCampaign(Long campaignId, Long customerId, Long policyId);

	/**
	 * @param campaignId
	 * @param customerId
	 * @param policyNo
	 * @param cb
	 * @return processInstance的id
	 */
	String joinCampaign(Long campaignId, Long customerId, String policyNo);

	/**
	 * 
	 * @param campaignName
	 * @param page
	 * @param size
	 * @return
	 */
	Page<VehicleRenewCampaignObjectEntity> findVehicleRenewCampaignObjectsByCampaign(String campaignName, int page,
			int size);

}
