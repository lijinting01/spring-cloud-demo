package cc.picc.mkt.service;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Justin
 *
 */
@Component
public class SendMessage implements TaskListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5599213823001706144L;

	private static final Logger logger = Logger.getLogger(SendMessage.class);

	@Override
	public void notify(DelegateTask delegateTask) {
		logger.info("发送消息" + delegateTask.getId() + "给" + delegateTask.getAssignee());
	}

}
