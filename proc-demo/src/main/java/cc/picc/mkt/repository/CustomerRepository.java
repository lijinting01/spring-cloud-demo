package cc.picc.mkt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import cc.picc.mkt.entity.CustomerEntity;

/**
 * 
 * @author lijinting01
 *
 */
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {
	/**
	 * 根据名称查找客户
	 * 
	 * @param name
	 * @param pageable
	 * @return
	 */
	Page<CustomerEntity> findByCustomerName(String name, Pageable pageable);

}
