package cc.picc.mkt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cc.picc.mkt.entity.BranchCampaignEntity;

/**
 * 
 * @author Justin
 *
 */
public interface BranchCampaignRepository extends JpaRepository<BranchCampaignEntity, Long> {
}
