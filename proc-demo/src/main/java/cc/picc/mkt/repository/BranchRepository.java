package cc.picc.mkt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cc.picc.mkt.entity.BranchEntity;

/**
 * 分公司
 * 
 * @author Justin
 *
 */
public interface BranchRepository extends JpaRepository<BranchEntity, Long> {

}
