package cc.picc.mkt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import cc.picc.mkt.entity.VehiclePolicyEntity;

/**
 * 
 * @author lijinting01
 *
 */
public interface VehiclePolicyEntityRepository extends JpaRepository<VehiclePolicyEntity, Long> {
	/**
	 * 
	 * @param policyNo
	 * @param pageable
	 * @return
	 */
	Page<VehiclePolicyEntity> findByPolicyNo(String policyNo, Pageable pageable);
}
