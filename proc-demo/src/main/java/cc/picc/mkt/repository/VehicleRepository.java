package cc.picc.mkt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import cc.picc.mkt.entity.VehicleEntity;

/**
 * 
 * @author lijinting01
 *
 */
public interface VehicleRepository extends JpaRepository<VehicleEntity, Long> {

	/**
	 * 
	 * @param licenseNumber
	 * @param pageable
	 * @return
	 */
	Page<VehicleEntity> findByLicenseNumber(String licenseNumber, Pageable pageable);

	/**
	 * 
	 * @param frameNumber
	 * @param pageable
	 * @return
	 */
	Page<VehicleEntity> findByFrameNumber(String frameNumber, Pageable pageable);

	/**
	 * 
	 * @param engineNumber
	 * @param pageable
	 * @return
	 */
	Page<VehicleEntity> findByEngineNumber(String engineNumber, Pageable pageable);

	/**
	 * 
	 * @param licenseNumber
	 * @param frameNumber
	 * @param engineNumber
	 * @param pageable
	 * @return
	 */
	Page<VehicleEntity> findByLicenseNumberAndFrameNumberAndEngineNumber(String licenseNumber, String frameNumber,
			String engineNumber, Pageable pageable);

}
