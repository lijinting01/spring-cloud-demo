package cc.picc.mkt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cc.picc.mkt.entity.VehicleRenewCampaignObjectEntity;

/**
 * 
 * @author lijinting01
 *
 */
public interface VehicleRenewCampaignObjectRepository extends JpaRepository<VehicleRenewCampaignObjectEntity, Long> {
	/**
	 * 
	 * @param name
	 * @param pageable
	 * @return
	 */
	@Query("SELECT o FROM VehicleRenewCampaignObjectEntity o WHERE o.campaign.campaignName = ?1")
	Page<VehicleRenewCampaignObjectEntity> findByCampaignName(String name, Pageable pageable);
}
