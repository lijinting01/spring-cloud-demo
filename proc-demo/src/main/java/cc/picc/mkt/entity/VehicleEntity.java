package cc.picc.mkt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import cc.picc.mkt.domain.VehicleInfo;

/**
 * 
 * @author lijinting01
 *
 */
@Entity
@Table(name = "mc_vehicle")
public class VehicleEntity implements VehicleInfo {

	@Id
	@GeneratedValue
	@Column(name = "vehicle_id")
	private Long id;

	@Column(name = "license_no", nullable = false)
	private String licenseNumber;

	@Column(name = "frame_no", nullable = false)
	private String frameNumber;

	@Column(name = "engine_no", nullable = false)
	private String engineNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "registered_date", nullable = false)
	private Date registeredDate;

	@Override
	public String toString() {
		return String.format("VehicleEntity [id=%s, licenseNumber=%s, frameNumber=%s, engineNumber=%s]", id,
				licenseNumber, frameNumber, engineNumber);
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public String getLicenseNumber() {
		return licenseNumber;
	}

	@Override
	public String getFrameNumber() {
		return frameNumber;
	}

	@Override
	public String getEngineNumber() {
		return engineNumber;
	}

	@Override
	public Date getRegisteredDate() {
		return registeredDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public void setFrameNumber(String frameNumber) {
		this.frameNumber = frameNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}
}
