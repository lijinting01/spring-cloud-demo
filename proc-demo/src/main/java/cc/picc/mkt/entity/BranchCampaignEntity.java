package cc.picc.mkt.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import cc.picc.mkt.domain.CampaignInfo;
import cc.picc.mkt.domain.CampaignType;

/**
 * 分公司的营销活动
 * 
 * @author Justin
 *
 */
@Entity
@Table(name = "rc_campaign")
public class BranchCampaignEntity implements CampaignInfo<BranchEntity> {

	@Id
	@GeneratedValue
	@Column(name = "campaign_id")
	private Long id;

	@OneToMany
	private List<BranchEntity> organizations = new ArrayList<>();

	@Column(name = "campaign_name", nullable = false)
	private String campaignName;

	@Enumerated(EnumType.STRING)
	@Column(name = "campaign_type", nullable = false, length = 30)
	private CampaignType campaignType;

	@Temporal(TemporalType.DATE)
	@Column(name = "deadline")
	private Date deadline;

	@Column(name = "goal", scale = 10)
	private Integer goal;

	/**
	 * 关联一个分公司
	 * 
	 * @param b
	 * @return
	 */
	public BranchCampaignEntity linkBranch(BranchEntity b) {
		this.organizations.add(b);
		return this;
	}

	/**
	 * 去掉一个关联
	 * 
	 * @param b
	 * @return
	 */
	public BranchCampaignEntity unlinkBranch(BranchEntity b) {
		this.organizations.remove(b);
		return this;
	}

	@Override
	public String toString() {
		return String.format("CampaignEntity [id=%s, campaignName=%s, campaignType=%s, deadline=%s, goal=%s]", id,
				campaignName, campaignType, deadline, goal);
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public List<BranchEntity> getOrganizations() {
		return Collections.unmodifiableList(organizations);
	}

	@Override
	public String getCampaignName() {
		return campaignName;
	}

	@Override
	public CampaignType getCampaignType() {
		return campaignType;
	}

	@Override
	public Date getDeadline() {
		return deadline;
	}

	@Override
	public Integer getGoal() {
		return goal;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOrganizations(List<BranchEntity> organizations) {
		this.organizations = organizations;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public void setCampaignType(CampaignType campaignType) {
		this.campaignType = campaignType;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public void setGoal(Integer goal) {
		this.goal = goal;
	}

}
