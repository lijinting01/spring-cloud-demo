package cc.picc.mkt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import cc.picc.mkt.domain.BranchLevel;
import cc.picc.mkt.domain.BranchLevelInfo;
import cc.picc.mkt.domain.OrganizationInfo;
import cc.picc.mkt.domain.OrganizationType;

/**
 * 分公司
 * 
 * @author Justin
 *
 */
@Entity
@Table(name = "um_branch")
public class BranchEntity implements OrganizationInfo, BranchLevelInfo {

	@Id
	@GeneratedValue
	@Column(name = "branch_id")
	private Long id;

	@Column(name = "level", nullable = false, length = 30)
	@Enumerated(EnumType.STRING)
	private BranchLevel branchLevel;

	@Column(name = "branch_name", nullable = false)
	private String organizationName;

	@Column(name = "branch_code", nullable = false, length = 20)
	private String organizationCode;

	@Enumerated(EnumType.STRING)
	@Column(name = "branch_type", length = 20, nullable = false)
	private OrganizationType organizationType;

	@Override
	public String toString() {
		return String.format(
				"BranchEntity [id=%s, branchLevel=%s, organizationName=%s, organizationCode=%s, organizationType=%s]",
				id, branchLevel, organizationName, organizationCode, organizationType);
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public BranchLevel getBranchLevel() {
		return branchLevel;
	}

	@Override
	public String getOrganizationName() {
		return organizationName;
	}

	@Override
	public String getOrganizationCode() {
		return organizationCode;
	}

	@Override
	public OrganizationType getOrganizationType() {
		return organizationType;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setBranchLevel(BranchLevel branchLevel) {
		this.branchLevel = branchLevel;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public void setOrganizationType(OrganizationType organizationType) {
		this.organizationType = organizationType;
	}

}
