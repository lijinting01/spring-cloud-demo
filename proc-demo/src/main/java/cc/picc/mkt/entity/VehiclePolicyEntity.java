package cc.picc.mkt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import cc.picc.mkt.domain.CustomerInfo;
import cc.picc.mkt.domain.VehiclePolicyInfo;

/**
 * 车险保单实体
 * 
 * @author lijinting01
 *
 */
@Entity
@Table(name = "mc_vehiclepolicy")
public class VehiclePolicyEntity implements VehiclePolicyInfo {

	@Id
	@GeneratedValue
	@Column(name = "vehicle_policy_id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "vehicle_id", nullable = false)
	private VehicleEntity vehicle;

	@ManyToOne
	@JoinColumn(name = "customer_id", nullable = false)
	private CustomerEntity customer;

	@Column(name = "proposal_no", nullable = false)
	private String proposalNo;

	@Column(name = "policy_no", nullable = false)
	private String policyNo;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date", nullable = false)
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date", nullable = false)
	private Date endDate;

	@Column(name = "class_code", length = 10)
	private String classCode;

	@Column(name = "risk_code", nullable = false, length = 10)
	private String riskCode;

	@Override
	public String toString() {
		return String.format(
				"VehiclePolicyEntity [id=%s, vehicle=%s, customer=%s, proposalNo=%s, policyNo=%s, startDate=%s, endDate=%s, classCode=%s, riskCode=%s]",
				id, vehicle, customer, proposalNo, policyNo, startDate, endDate, classCode, riskCode);
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public VehicleEntity getVehicle() {
		return vehicle;
	}

	@Override
	public CustomerInfo getCustomer() {
		return customer;
	}

	@Override
	public String getProposalNo() {
		return proposalNo;
	}

	@Override
	public String getPolicyNo() {
		return policyNo;
	}

	@Override
	public Date getStartDate() {
		return startDate;
	}

	@Override
	public Date getEndDate() {
		return endDate;
	}

	@Override
	public String getClassCode() {
		return classCode;
	}

	@Override
	public String getRiskCode() {
		return riskCode;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setVehicle(VehicleEntity vehicle) {
		this.vehicle = vehicle;
	}

	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

}
