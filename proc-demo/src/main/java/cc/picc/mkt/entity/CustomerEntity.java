package cc.picc.mkt.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import cc.picc.mkt.domain.CustomerInfo;
import cc.picc.mkt.domain.CustomerType;

/**
 * 
 * @author lijinting01
 *
 */
@Entity
@Table(name = "mc_customer")
public class CustomerEntity implements CustomerInfo {

	/**
	 * 
	 */
	@Id
	@GeneratedValue
	@Column(name = "customer_id")
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "customer_type", length = 20, nullable = false)
	private CustomerType customerType;

	@Column(name = "customer_name", nullable = false)
	private String customerName;

	@Override
	public String toString() {
		return String.format("CustomerEntity [id=%s, customerType=%s, customerName=%s]", id, customerType,
				customerName);
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public CustomerType getCustomerType() {
		return customerType;
	}

	@Override
	public String getCustomerName() {
		return customerName;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCustomerType(CustomerType customerType) {
		this.customerType = customerType;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

}
