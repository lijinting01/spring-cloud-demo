package cc.picc.mkt.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cc.picc.mkt.domain.CustomerInfo;
import cc.picc.mkt.domain.RenewCampaignObjectInfo;
import cc.picc.mkt.domain.VehicleRenewCampaignObjectInfo;

/**
 * 车险续保对象
 * 
 * @author lijinting01
 *
 */
@Entity
@Table(name = "rc_vehiclecampaignobject")
public class VehicleRenewCampaignObjectEntity
		implements VehicleRenewCampaignObjectInfo, RenewCampaignObjectInfo<BranchEntity> {

	@Id
	@GeneratedValue
	@Column(name = "object_id")
	private Long id;

	@JoinColumn(name = "campaign_id", nullable = false)
	@ManyToOne
	private BranchCampaignEntity campaign;

	@JoinColumn(name = "customer_id", nullable = false)
	@ManyToOne
	private CustomerEntity customer;

	@JoinColumn(name = "vehicle_id", nullable = false)
	@ManyToOne
	private VehicleEntity vehicle;

	@JoinColumn(name = "vehicle_policy_id", nullable = false)
	@ManyToOne
	private VehiclePolicyEntity vehiclePolicy;

	@Column(name = "insert_time")
	private Date insertTime;

	/**
	 * 构造函数会初始化insertTime
	 */
	public VehicleRenewCampaignObjectEntity() {
		this.insertTime = new Date();
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public BranchCampaignEntity getCampaign() {
		return campaign;
	}

	@Override
	public VehicleEntity getVehicle() {
		return vehicle;
	}

	@Override
	public VehiclePolicyEntity getPolicy() {
		return vehiclePolicy;
	}

	@Override
	public Date getInsertTime() {
		return insertTime;
	}

	@Override
	public CustomerInfo getCustomer() {
		return customer;
	}

	public VehiclePolicyEntity getVehiclePolicy() {
		return vehiclePolicy;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCampaign(BranchCampaignEntity campaign) {
		this.campaign = campaign;
	}

	public void setCustomer(CustomerEntity customer) {
		this.customer = customer;
	}

	public void setVehicle(VehicleEntity vehicle) {
		this.vehicle = vehicle;
	}

	public void setVehiclePolicy(VehiclePolicyEntity vehiclePolicy) {
		this.vehiclePolicy = vehiclePolicy;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

}
