package cc.picc;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.joda.time.DateTime;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import cc.picc.mkt.domain.BranchLevel;
import cc.picc.mkt.domain.CampaignType;
import cc.picc.mkt.domain.CustomerType;
import cc.picc.mkt.domain.OrganizationType;
import cc.picc.mkt.entity.BranchCampaignEntity;
import cc.picc.mkt.entity.BranchEntity;
import cc.picc.mkt.entity.CustomerEntity;
import cc.picc.mkt.entity.VehicleEntity;
import cc.picc.mkt.entity.VehiclePolicyEntity;
import cc.picc.mkt.repository.BranchCampaignRepository;
import cc.picc.mkt.repository.BranchRepository;
import cc.picc.mkt.repository.CustomerRepository;
import cc.picc.mkt.repository.VehiclePolicyEntityRepository;
import cc.picc.mkt.repository.VehicleRepository;

/**
 * 
 * @author lijinting01
 *
 */
@Configuration
public class Bootstrap {

	/**
	 * 
	 * @return
	 */
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	/**
	 * 
	 * @param vehicleRepo
	 * @param vehiclePolicyRepo
	 * @param customerRepo
	 * @param branchRepo
	 * @param branchCampaignRepo
	 * @return
	 */
	@Bean
	CommandLineRunner init(VehicleRepository vehicleRepo, VehiclePolicyEntityRepository vehiclePolicyRepo,
			CustomerRepository customerRepo, BranchRepository branchRepo, BranchCampaignRepository branchCampaignRepo) {
		return (String... args) -> {
			BranchCampaignEntity bce = randomCampaign();

			List<BranchEntity> ll = newBranches();
			for (BranchEntity e : ll) {
				BranchEntity ee = branchRepo.save(e);
				bce.linkBranch(ee);
			}

			branchCampaignRepo.save(bce);

			for (int i = 0; i < 10; i++) {
				VehicleEntity vehicle = vehicleRepo.save(randomVehicle());
				CustomerEntity customer = customerRepo.save(randomCustomer());
				vehiclePolicyRepo.save(randomVehiclePolicy(vehicle, customer));
			}
		};
	}

	static List<BranchEntity> newBranches() {
		BranchEntity b1 = new BranchEntity();
		b1.setBranchLevel(BranchLevel.MUNICIPAL);
		b1.setOrganizationCode("44060000");
		b1.setOrganizationName("佛山市分公司");
		b1.setOrganizationType(OrganizationType.BRANCH);

		BranchEntity b2 = new BranchEntity();
		b2.setBranchLevel(BranchLevel.MUNICIPAL);
		b2.setOrganizationCode("44020000");
		b2.setOrganizationName("东莞市分公司");
		b2.setOrganizationType(OrganizationType.BRANCH);
		return Arrays.asList(b1, b2);
	}

	static BranchCampaignEntity randomCampaign() {
		BranchCampaignEntity campaign = new BranchCampaignEntity();
		campaign.setCampaignName("测试活动");
		campaign.setCampaignType(CampaignType.VEHICLE_RENEW);
		campaign.setDeadline(DateTime.now().plusYears(1).toDate());
		campaign.setGoal(RandomUtils.nextInt(100, 200));

		return campaign;
	}

	static VehicleEntity randomVehicle() {
		VehicleEntity vehicle = new VehicleEntity();
		vehicle.setLicenseNumber("粤" + RandomStringUtils.randomAlphanumeric(5).toUpperCase());
		vehicle.setFrameNumber(RandomStringUtils.randomAlphabetic(16));
		vehicle.setEngineNumber(RandomStringUtils.randomAlphabetic(8));
		vehicle.setRegisteredDate(DateTime.now().minusYears(1).toDate());
		return vehicle;
	}

	static CustomerEntity randomCustomer() {
		CustomerEntity customer = new CustomerEntity();
		customer.setCustomerType(CustomerType.INDIVIDUAL);
		customer.setCustomerName(RandomStringUtils.randomAlphabetic(9));
		return customer;
	}

	static VehiclePolicyEntity randomVehiclePolicy(VehicleEntity vehicle, CustomerEntity customer) {
		VehiclePolicyEntity entity = new VehiclePolicyEntity();
		entity.setPolicyNo("PDAA" + RandomStringUtils.randomNumeric(18));
		entity.setProposalNo("TDAA" + RandomStringUtils.randomNumeric(18));
		entity.setRiskCode("DAA");
		entity.setClassCode("05");
		entity.setVehicle(vehicle);
		entity.setStartDate(DateTime.now().toDate());
		entity.setEndDate(DateTime.now().plusYears(1).toDate());
		entity.setCustomer(customer);
		return entity;
	}
}
