package cc.picc.commons;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Excel中单个sheet中的单元格
 * 
 * @author Justin
 *
 */
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface Cell {
	/**
	 * 单元格的 cellIndex，从0开始计数。 如果为-1则表示当前属性无效，应当予以忽略。
	 * 
	 * @return
	 */
	int index() default -1;

	/**
	 * 单元格对应的title。适用于通过标题行定位所要取得单元格的情况。 如果为空字符串或null则表示当前属性无效，应当予以忽略。
	 * 
	 * @return
	 */
	String title() default "";
}
