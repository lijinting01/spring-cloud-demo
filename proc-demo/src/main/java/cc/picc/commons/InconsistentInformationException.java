package cc.picc.commons;

/**
 * 
 * @author lijinting01
 *
 */
public class InconsistentInformationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9207196269195157476L;

	/**
	 * 
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public InconsistentInformationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public InconsistentInformationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * 
	 * @param message
	 */
	public InconsistentInformationException(String message) {
		super(message);
	}

	/**
	 * 
	 * @param cause
	 */
	public InconsistentInformationException(Throwable cause) {
		super(cause);
	}

}
