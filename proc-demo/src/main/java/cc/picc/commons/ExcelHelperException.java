package cc.picc.commons;

/**
 * 
 * @author Justin
 *
 */
public class ExcelHelperException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8976207603154539407L;

	/**
	 * ExcelHelperException
	 */
	public ExcelHelperException() {
		super();
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ExcelHelperException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public ExcelHelperException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * 
	 * @param message
	 */
	public ExcelHelperException(String message) {
		super(message);
	}

	/**
	 * 
	 * @param cause
	 */
	public ExcelHelperException(Throwable cause) {
		super(cause);
	}

}
