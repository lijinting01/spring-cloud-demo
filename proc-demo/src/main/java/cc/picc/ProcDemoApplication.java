package cc.picc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 
 * @author lijinting01
 *
 */
@EnableEurekaClient
@SpringBootApplication
public class ProcDemoApplication {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(ProcDemoApplication.class, args);
	}
}
