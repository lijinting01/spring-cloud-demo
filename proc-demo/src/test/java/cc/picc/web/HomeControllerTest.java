package cc.picc.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringJUnit4ClassRunner.class)
public class HomeControllerTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testHome() throws Exception {
		HomeController ctrl = new HomeController();
		MockMvc mockMvc = standaloneSetup(ctrl).build();
		mockMvc.perform(get("/")).andExpect(view().name("home"));
	}

}
