package cc.picc.web;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import cc.picc.commons.InconsistentInformationException;
import cc.picc.mkt.service.VehicleRenewCampaignService;

public class VehicleRenewCampaignControllerTest {

	private VehicleRenewCampaignController ctrl;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testJoinCampaign() throws Exception {
		VehicleRenewCampaignService mock = mock(VehicleRenewCampaignService.class);
		when(mock.joinCampaign(anyLong(), anyLong(), anyString())).thenReturn("10086");
		this.ctrl = new VehicleRenewCampaignController(mock);

		MockMvc mockMvc = standaloneSetup(ctrl).build();
		mockMvc.perform(
				post("/api/campaign/vehicle/renew/join").param("campaignId", "1").param("customerId", "10").param("policyNo", "312432344"))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.processInstanceId", is("10086")));
	}

	/**
	 * 不满足中间结果约束——找不到结果
	 * 
	 * @throws Exception
	 */
	@Test
	public void testJoinCampaignThrowsEmptyResultDataAccessException() throws Exception {
		VehicleRenewCampaignService mock = mock(VehicleRenewCampaignService.class);
		when(mock.joinCampaign(anyLong(), anyLong(), anyString()))
				.thenThrow(new EmptyResultDataAccessException("123456", 1));
		this.ctrl = new VehicleRenewCampaignController(mock);

		MockMvc mockMvc = standaloneSetup(ctrl).build();
		mockMvc.perform(
				post("/api/campaign/vehicle/renew/join").param("campaignId", "1").param("customerId", "10").param("policyNo", "312432344"))
				.andExpect(status().isNotFound()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.code", is(equalTo(-5001))));
	}

	/**
	 * 不满足中间结果约束——信息不一致
	 * 
	 * @throws Exception
	 */
	@Test
	public void testJoinCampaignThrowsInconsistentInformationException() throws Exception {
		VehicleRenewCampaignService mock = mock(VehicleRenewCampaignService.class);
		when(mock.joinCampaign(anyLong(), anyLong(), anyString()))
				.thenThrow(new InconsistentInformationException("123456"));
		this.ctrl = new VehicleRenewCampaignController(mock);

		MockMvc mockMvc = standaloneSetup(ctrl).build();
		mockMvc.perform(
				post("/api/campaign/vehicle/renew/join").param("campaignId", "1").param("customerId", "10").param("policyNo", "312432344"))
				.andExpect(status().isPreconditionFailed())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.code", is(equalTo(-5002))));
	}

	@Test
	@Ignore
	public void testHandleEmptyResultDataAccessException() {
		fail("Not yet implemented");
	}

	@Test
	@Ignore
	public void testHandleInconsistentInformationException() {
		fail("Not yet implemented");
	}

}
